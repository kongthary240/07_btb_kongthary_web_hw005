import React, { useState } from "react";

function Content({ datas, setData, getVawhenbtncick }) {
  const [newData, setNewData] = useState([{}]);

  const inputHanlder = (event) => {
    setNewData({ ...newData, [event.target.name]: event.target.value });
  };

  const submit = (e) => {
    e.preventDefault();
    setData([...datas, { id: datas.length + 1, ...newData }]);
  };

  const handebeachbtn = (e) => {
    getVawhenbtncick(e);
  };
  return (
    <div>
      <div>
        <div className="">
          <h6 class="  text-left text-3xl font-bold mb-0 ">
            Good Evening Team!
          </h6>
          <label
            htmlFor="my-modal-3"
            className="btn bg-red-500 hover:bg-bue-400 "
          >
            ADD NEW TRIP
          </label>
        </div>

        {/* Put this part before </body> tag */}
        <input type="checkbox" id="my-modal-3" className="modal-toggle" />
        <div className="modal">
          <div className="modal-box relative bg-white">
            <label
              htmlFor="my-modal-3"
              className="btn btn-sm btn-circle absolute right-2 top-2"
            >
              {" "}
              ✕{" "}
            </label>

            <form className="bg-white" onSubmit={submit}>
              {/* Title */}

              <div className="block text-gray-700 text-sm font-bold mb-2">
                <label
                  class=" text-black-300 text-sm font-bold mb-2"
                 
                >
                  Title
                </label>
                <input
                  onChange={inputHanlder}
                  class="shadow  text-black appearance-none border bg-color rounded w-full bg-emerald-300 py-2 px-3 "
                  name="title"
                  type="text"
                  placeholder="Title"
                />

                {/* Description */}
                <div>
                  <label
                    class="block text-sm font-bold mb-2 text-black"
                
                  >
                    Description
                  </label  >
                  <input
                    onChange={inputHanlder}
                    class="shadow appearance-none border rounded w-full bg-color bg-lime-300 py-2 px-3 text-black "
                    name="description"
                    type="text"
                    placeholder="Description"
                  />
                </div>
               
                <div>
                  <label class="block  text-sm font-bold mb-2 text-black">
                    People going{" "}
                  </label>

                  <input
                    onChange={inputHanlder}
                    class="shadow appearance-none border rounded w-full bg-color bg-yellow-100 text-black py-2 px-3  "
                    name="peopleGoing"
                    type="text"
                    placeholder="People going"
                  />
                </div>
                {/* Type of adventure */}
                <label class="block text-gray-700 text-sm font-bold mb-2">
                  Type of adventure
                </label>
                <select
                  onChange={inputHanlder}
                  name="status"
                  className="select select-info w-full bg-pink-100 text-black py-2 leading-tight focus:outline-none focus:shadow-outline"
                >
                  <option disabled selected className="text-black">
                    Select language
                  </option>
                  <option value="mountain">mountain</option>
                  <option value="forest">forest</option>
                  <option value="beach">beach</option>
                </select><br></br><br></br>
                <button type="submit" className="btn btn-primary">
                  Button
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div className="flex flex-wrap m-4 ">
        {datas.map((e) => (
          <div className="card  bg-neutral text-neutral-content w-80 m-1">
            <div className="card-body items-center text-center">
              <h2 className="card-title">{e.title}</h2>
              <p className="line-clamp-3">{e.description}</p>
              <p className="line-clamp-3">{e.status}</p>
              <p className="line-clamp-3">{e.peopleGoing}</p>

              <div className="card-actions justify-end">
                <button
                  onClick={() => handebeachbtn(e)}
                  className={`${e.status === "beach" && "bg-blue-500"} ${
                    e.status === "mountain" && "bg-red-500"
                  } ${
                    e.status === "forest" && "bg-green-500"
                  } px-3 py-2 w-[5rem] rounded-2xl text-center`}
                >
                  {e.status === "beach" && "beach"}
                  {e.status === "mountain" && "mountain"}
                  {e.status === "forest" && "forest"}
                </button>
                {/* The button to open modal */}
                <label htmlFor="my-modal-4" className="btn">
                  Read detail
                </label>

                {/* Put this part before </body> tag */}
                <input
                  type="checkbox"
                  id="my-modal-4"
                  className="modal-toggle"
                />
                <div className="modal">
                  <div className="modal-box relative">
                    <label
                      htmlFor="my-modal-4"
                      className="btn btn-sm btn-circle absolute right-2 top-2"
                    >
                      ✕
                    </label>
                    <h3 className="text-lg font-bold"></h3>
                    <p className="py-4"></p>
                    <h2 className="card-title">{e.title}</h2>
                    <p className="line-clamp-3">{e.description}</p>
                    <p className="line-clamp-3">{e.status}</p>
                    <p className="line-clamp-3">{e.peopleGoing}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
export default Content;
