import React from 'react'
import Content from './Content'
import Icon from './Icon'
import Sideleft from './Sideleft'

function Home({datas , setData, getVawhenbtncick}) {
 


  return (
    <div className="grid grid-cols-12 h-screen w-screen">
    <div className="col-span-1 bg-slate-200">
      <Icon/>
    </div>
    <div className="col-span-8 bg-white">
      <Content datas={datas} setData={setData} getVawhenbtncick={getVawhenbtncick}/>
    </div>
    <div className="col-span-3 bg-green-300">
      <Sideleft/>
    </div>
  </div>
  )
}

export default Home