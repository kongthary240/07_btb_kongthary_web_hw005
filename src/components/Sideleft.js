import React from "react";
import notification from "../Images/notification.png"
import comment from "../Images/comment.png"
import lachlan from "../Images/lachlan.jpg"
import raamin from "../Images/raamin.jpg"
import nonamesontheway  from "../Images/nonamesontheway.jpg"
import christina  from "../Images/christina.jpg"

function Sideleft() {
  return (
    <div >
      <div className="flex justify-between ml-[214px] mt-5 items-center w-32">
        <div className="mb-3 w-5">
          <img src={notification}></img>
        </div>
        <div className="mb-3 w-5">
          <img src={comment}></img>
        </div>
        <div className="mb-3 w-5">
          <img src={lachlan}></img>
        </div>
      </div>
      
      <div className="text-black">
        <button class="bg-orange-100 hover:bg-blue-70
         font-bold py-2 px-2 rounded  m-auto w-[150px] ml-[150px]">
          My amazing trip
        </button>
      </div>
      <div className="text-4xl text-white">
        <h3>I like laying down on <br/>
          the sand and looking at <br/>
          the moon</h3>
      </div>

      <div className="text-white" >
        
          <p>27 people going to this trip</p>
        
      </div> <br/>
      <div className="flex space-x-4  items-center">
        <div className="flex mb-3 ml-4 ">
          <img className="h-10 w-10 rounded-full" src={lachlan}></img>
        </div>
        <div className="mb-3 ">
          <img className="h-10 w-10 rounded-full" src={raamin}></img>
        </div>
        <div className="mb-3 ">
          <img className="h-10 w-10 rounded-full" src={nonamesontheway}></img>
        </div>
        <div className="mb-3">
        <img className="h-10 w-10 rounded-full" src={christina}></img>
        </div>
      </div>
     
   
    </div>
  );
}

export default Sideleft;
