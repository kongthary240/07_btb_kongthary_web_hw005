import React from "react";
import christina  from "../Images/christina.jpg"
import notification from "../Images/notification.png"
import comment from "../Images/comment.png"
import lachlan from "../Images/lachlan.jpg"
import raamin from "../Images/raamin.jpg"
import nonamesontheway  from "../Images/nonamesontheway.jpg"
import category  from "../Images/category_icon.png"
import  cube from "../Images/cube.png"
import list  from "../Images/list.png"
import messenger  from "../Images/messenger.png"
import success  from "../Images/success.png"
import  security from "../Images/security.png"
import users  from "../Images/users.png"
import plus  from "../Images/plus.png"







function Icon() {
  return (
    <>
      <div className="flex flex-col justify-center items-center m-auto">
        <div className="mb-7 rounded-full h-10 w-10">
          <img src={category}></img>
        </div>
        <div className="mb-3 rounded-full h-10 w-10">
          <img src={cube}></img>
        </div>
        <div className="mb-3 rounded-full h-10 w-10">
          <img src={list}></img>
        </div>
        <div className="mb-3 rounded-full h-10 w-10">
          <img src={messenger}></img>
        </div>
        <div className="mb-7 rounded-full h-10 w-10">
          <img src={notification}></img>
        </div>
        <div className="mb-3 rounded-full h-10 w-10">
          <img src={success}></img>
        </div>
        <div className="mb-3 rounded-full h-10 w-10">
        <img src={security}></img>
        </div>
        <div className="mb-7 rounded-full h-10 w-10">
          <img src={users}></img>
        </div>
        <div className="mb-3 rounded-full h-10 w-10">
          <img src={users} className="rounded-full h-10 w-10"></img>
        </div>
        <div className="mb-3">
          <img src={raamin} className="rounded-full h-10 w-10"></img>
        </div>
        <div className="mb-3">
          <img src={christina}className="rounded-full h-10 w-10"></img>
        </div>
        <div className="mb-3 rounded-full h-10 w-10">
          <img src={plus }></img>
        </div>
      </div>
    </>
  );
}

export default Icon;
